package superpower;

public class Superpower {
    private final int healValue;

    public Superpower(int healValue) {
        this.healValue = healValue;
    }

    public int heal() {
        return this.healValue;
    }
}
