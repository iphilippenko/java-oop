package weapon;

public class Weapon {
    private String name;
    private final int hitPoints;

    public Weapon(String name, int hitPoints) {
        this.name = name;
        this.hitPoints = hitPoints;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int makeHit() {
        return this.hitPoints;
    }
}
