import superpower.Superpower;
import weapon.Weapon;
import character.Character;

public class Main {
    public static void main(String[] args) {
        Weapon sword = new Weapon("Sword", 15);
        Weapon knife = new Weapon("Knife", 5);
        Weapon bowAndArrows = new Weapon("Bow and Arrows", 10);

        Superpower elfSuperpower = new Superpower(10);
        Superpower knightSuperpower = new Superpower(15);
        Superpower goblinSuperpower = new Superpower(5);

        Character knight = new Character("Knight", 100, sword, knightSuperpower);
        Character elf = new Character("Elf", 80, bowAndArrows, elfSuperpower);
        Character goblin = new Character("Goblin", 80, goblinSuperpower);

        elf.fight(knight);
        knight.fight(goblin);
    }
}
