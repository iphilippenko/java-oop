package character;

import superpower.Superpower;
import weapon.Weapon;

public class Character {
    private String name;
    private int health;
    private Weapon weapon;
    private final Superpower superpower;

    public Character(String name, int health, Superpower superpower) {
        this.name = name;
        this.health = health;
        this.superpower = superpower;
    }

    public Character(String name, int health, Weapon weapon, Superpower superpower) {
        this.name = name;
        this.health = health;
        this.weapon = weapon;
        this.superpower = superpower;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getHealth() {
        return this.health;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Weapon getWeapon() {
        return this.weapon;
    }

    public boolean hasWeapon() {
        return this.weapon != null;
    }

    private void useHeal() {
        int healValue = this.superpower.heal();
        this.health += healValue;
        System.out.println(this.name + " uses special skill and increases health in " + healValue + " points");
    }


    public void fight(Character opponent) {
        System.out.println(name + " wants to fight with" + opponent.getName());

        if (!opponent.hasWeapon()) {
            System.out.println(opponent.name + " has no weapon and running away...");
            return;
        }

        this.useHeal();

        System.out.println(name + " makes a hit with " + weapon.getName());

        int damage = weapon.makeHit();

        System.out.println(opponent.getName() + " looses " + damage + " health points");

        opponent.setHealth(opponent.getHealth() - damage);

        System.out.println(opponent.getName() + " remaining health is " + opponent.getHealth());

        if (opponent.health <= 0) {
            System.out.println(opponent.name + " is defeated!");
        }
    }

}

